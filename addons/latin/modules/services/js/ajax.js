/*
 * @author Luis Fernando Salazar
 * @description 
 */
$(document).ready(function()
{
	var base_url = $('#baseurl').html();  // seleccionamos la base url de un div
	var selCategory = $('#selCategory').html();  // seleccionamos la categoria si tiene alguna
	
	$(document).on("click ", "#more_items_ajax", function(event)
	{
		var page_ajax = $('#page_ajax').html();
		$.ajax({
			type: "POST",
			url: base_url+'services/ajax_items/'+selCategory+'/',
			data: ({page_ajax: page_ajax}),
			beforeSend: function () {
			    //$("#selectDepDiv").html('<img src="'+base_url+'images/spinner.gif" width="28" height="28"/>');
			},
			success: function(html){
				$("#upload_items").append(html);
				$('#page_ajax').html( parseInt(page_ajax) + 1);
			},
			error: function(err)
	        {
	        	alert("Ocurrió un error. Por favor inténtelo de nuevo.");
	        }
		});
	});
});