<?php if($services): ?>
    <?php foreach ($services as $service): ?>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div style="overflow: hidden;max-height:170px;">
                    <img src="<?php echo $service->image; ?>" data-src="holder.js/300x200" width="100%" alt="" class="img-responsive">
                </div>
                <div class="caption">
                    <h4><?php echo $service->name ?></h4>
                    <p><?php echo $service->introduction ?></p>
                    <p><a class="btn btn-primary btn-sm" href="<?php echo $service->url ?>" >Ver Mas</a></p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="col-sm-12 col-md12"><p style="text-align:center;margin-top:80px"><strong>No se encontraron más resultados...</strong></p></div>
<?php endif; ?>