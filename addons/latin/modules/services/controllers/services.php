<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
*
* @author 	    Brayan Acebo
* @package 	PyroCMS
* @subpackage 	services
* @category 	Modulos
*/

class services extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $models = array(
            "service_model",
            "service_category_model",
            "service_image_model",
            "service_intro_model"
            );
        $this->load->model($models);
		$this->template
                ->append_js('module::jquery.min.js')
                ->append_js('module::ajax.js');
		$this->numer_per_pages = 15;
    }

// -----------------------------------------------------------------

    public function index($selCategory = null)
    {
        $category = null;
        if($selCategory){
            $category = $this->service_category_model->get_many_by("slug",$selCategory);
            if (count($category) > 0) {
            $category = $category[0]; // Categoria Seleccionada

            $return = $this->db->limit($this->numer_per_pages, 0)->where('category_id',$category->id)->get('services_categories')->result();
            $selected_category = array();
            $services= array();
            $selected_services = array();

            foreach ($return as $item) {
                $selected_services[] = $item->service_id;
            }

            if(count($selected_services) > 0){
                for ($i=0;$i<count($selected_services);$i++) {
                // Se consulta serviceo a serviceo de la categoria
                    $service = $this->service_model->get_many_by("id",$selected_services[$i])[0];
                // Llenamos el nuevo objeto con todos los servicios
                    $servicesObj = new stdClass();
                    $servicesObj->id = $service->id;
                    $servicesObj->name = substr($service->name, 0, 20);
                    $servicesObj->image = val_image($service->image);
                    $servicesObj->introduction = substr($service->introduction, 0, 100);
                    $servicesObj->price = ($service->price) ? "Precio: $".number_format($service->price) : null;
                    $servicesObj->slug = $service->slug;
                    $servicesObj->url = site_url('services/detail/'.$service->slug);
                $services[] = $servicesObj; // Array de objectos
            }

                // Se ordena el Array por id de forma DESC
            usort($services, function($a, $b)
            {
                return strcmp($b->id, $a->id);
            });
        }

    }else{
                redirect("services"); // si la categoria no existe me redirecciona a todos
            }
        }else{

        // Se consultan los servicios
            $services = $this->service_model
            ->limit($this->numer_per_pages, 0)
            ->order_by('id', 'DESC')
            ->get_all();

            foreach($services AS $item)
            {
                $item->name = substr($item->name, 0, 20);
                $item->image = val_image($item->image);
                $item->introduction = substr($item->introduction, 0, 100);
                $item->price = ($item->price) ? "Precio: $".number_format($item->price) : null;
                $item->url = site_url('services/detail/'.$item->slug);
            }
        }

    // Consultamos las categorias
        $categories = $this->service_category_model
        ->order_by('title', 'ASC')
        ->get_all();

    // Intro
        $in = $this->service_intro_model->get_all();
        $intro = array();
        if (count($in) > 0) {
            $intro = $in[0];
        }

    // Devuelve arbol en HTML, el segundo parametro es el nombre del modulo
        $menu = treemenu($categories,'services');

        $this->template
        ->set('services', $services)
        ->set('category', ($category) ? "/ ".$category->title : null)
        ->set('categories', $categories)
        ->set('menu', $menu)
        ->set('intro', $intro)
		->set('selCategory', $selCategory)
        ->build('index');
    }
	
	public function ajax_items($selCategory = null)
    {
    	$page = (isset($_POST['page_ajax']) ? $_POST['page_ajax'] : 1);
        $category = null;
        if($selCategory){
            $category = $this->service_category_model->get_many_by("slug",$selCategory);
            if (count($category) > 0) {
            $category = $category[0]; // Categoria Seleccionada

            $return = $this->db->limit($this->numer_per_pages, $page*$this->numer_per_pages)->where('category_id',$category->id)->get('services_categories')->result();
            $selected_category = array();
            $services= array();
            $selected_services = array();

            foreach ($return as $item) {
                $selected_services[] = $item->service_id;
            }

            if(count($selected_services) > 0){
                for ($i=0;$i<count($selected_services);$i++) {
                // Se consulta serviceo a serviceo de la categoria
                    $service = $this->service_model->get_many_by("id",$selected_services[$i])[0];
                // Llenamos el nuevo objeto con todos los servicios
                    $servicesObj = new stdClass();
                    $servicesObj->id = $service->id;
                    $servicesObj->name = substr($service->name, 0, 20);
                    $servicesObj->image = val_image($service->image);
                    $servicesObj->introduction = substr($service->introduction, 0, 100);
                    $servicesObj->price = ($service->price) ? "Precio: $".number_format($service->price) : null;
                    $servicesObj->slug = $service->slug;
                    $servicesObj->url = site_url('services/detail/'.$service->slug);
                $services[] = $servicesObj; // Array de objectos
            }

                // Se ordena el Array por id de forma DESC
            usort($services, function($a, $b)
            {
                return strcmp($b->id, $a->id);
            });
        }

    }else{
                redirect("services"); // si la categoria no existe me redirecciona a todos
            }
        }else{

        // Se consultan los servicios
            $services = $this->service_model
            ->limit($this->numer_per_pages, $page*$this->numer_per_pages)
            ->order_by('id', 'DESC')
            ->get_all();

            foreach($services AS $item)
            {
                $item->name = substr($item->name, 0, 20);
                $item->image = val_image($item->image);
                $item->introduction = substr($item->introduction, 0, 100);
                $item->price = ($item->price) ? "Precio: $".number_format($item->price) : null;
                $item->url = site_url('services/detail/'.$item->slug);
            }
        }
		
		$dataView['services'] = $services;
		$this->template->set_layout(FALSE);
		$this->template->build('items_ajax', $dataView);
		//$this->load->view('items_ajax', $dataView);
    }

// ----------------------------------------------------------------------

    public function detail($slug)
    {

        $return = $this->service_model->get_many_by('slug', $slug)[0];

        if(!$return)
            redirect('services');

        // Se convierten algunas variables necesarias para usar como slugs
        $setter = array(
            'image' => val_image($return->image),
            'price' => ($return->price) ? "Precio: $".number_format($return->price) : null
            );

        $service = array_merge((array)$return,$setter);

        $relation = $this->db->where('service_id',$service['id'])->get('services_categories')->result();
        $categories = array();
        foreach ($relation as $item) {
            $category = $this->service_category_model->get_many_by('id', $item->category_id)[0];
            $categories[] = array(
                    "title" => $category->title,
                    "slug" => $category->slug
                );
        }

        // imagenes para slider
        $images = $this->service_image_model->get_many_by('service_id',$service['id']);

        $this->template
                ->set('service', (object) $service)
                ->set('categories', $categories)
                ->set('images', $images)
                ->build('detail');

    }
}