<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Brayan Acebo
 */
class Gallery extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $models = array(
            'gallery_model',
            'gallery_intro_model'
            );
        $this->load->model($models);
    }

    // -----------------------------------------------------------------

    public function index() {

        // Paginación
        $pagination = create_pagination('gallery/index', $this->gallery_model->count_all(), 16, 3);

        // Galeria
        $gallery = $this->gallery_model
        ->limit($pagination['limit'], $pagination['offset'])
        ->order_by('id', 'DESC')
        ->get_all();

        foreach($gallery AS $item)
        {
            if($item->type == 1){
                $item->content = val_image($item->content);
                $item->title = substr($item->title, 0,200);
            }else{
                $item->content = html_entity_decode($item->content);
            }
        }

        // Intro
        $in = $this->gallery_intro_model->get_all();
        $intro = array();

        if (count($in) > 0) {
            $intro = $in[0];
        }

        $this->template
        ->set('pagination', $pagination['links'])
        ->set('gallery', $gallery)
        ->set('intro', $intro)
        ->build('index');
    }

}