<div class="row">
	<!-- TITULO -->
	<div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
		<h1>Noticias</h1>
	</div>
	<br>
</div>

<div class="container-fluid">
	{{ news }}
	<div class="row">
		<hr>
		<div class=" col-sx-12 col-sm-4 col-md-4 col-lg-4">
			<div class="cont-img-blog">
				<img src="{{ image }}" alt="">
			</div>
		</div>
		<div class=" col-sx-12 col-sm-8 col-md-8 col-lg-8">
			<h2><a href="{{ urlDetail }}">{{ title }}</a></h2>
			<p>{{ introduction }}</p>
			<span class="post-date tx-al-ri">{{ date }}</span>
			<a class="btn btn-primary mtop1" href="{{ urlDetail }}">Ver Mas</a>
		</div>
	</div>
	{{ /news }}
	{{ pagination }}
</div>

