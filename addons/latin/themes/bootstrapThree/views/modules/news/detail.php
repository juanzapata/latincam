<div class="row">
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12 tx-al-ri">
        <a class="btn btn-primary btn-sm back" href="javascript:history.back(1)"><span class="glyphicon glyphicon-arrow-left"></span> Volver</a>
    </div>
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
        <h1>{{ data.title }}</h1>
    </div>
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12 mbot1">
        <div class="post-img"><img src="{{ data.image }}" alt=""></div>
        <p>{{ data.content }}</p>
        <span class="post-date tx-al-ri">{{ data.date }}</span>
    </div>
</div>